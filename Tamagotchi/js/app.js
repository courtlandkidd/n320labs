/**
 * Created by Courtland on 8/31/2017.
 */

//class not fully supported yet
class Tamagotchi {

    //constructs this object - helps to get it set up
    constructor(name, type, energy, selector) {
        this.name = name;
        this.type = type;
        this.energy = energy;
        this.element = document.querySelector(selector);

        //start the tamagotchi simulation
        setInterval(this.update.bind(this), 1000);
    }

    report() {
        console.log(this.name + " is a " + this.type + " and has e" + this.energy);
    }

    //add energy to tamagotchi
    eat() {
        //add one energy
        this.energy += 1;
    }

    //common video game method
    //runs the tamagotchi 'sim'
    update () {
        this.energy --;
        this.element.innerHTML = this.name + " has " + this.energy + " energy";

        if (myTamagotchi.energy < 1) {
            document.write("Theo has run away!")
        }
    }
}

//make an instance of a tamagotchi
var myTamagotchi = new Tamagotchi("Theo", "heart", 24, "#dvInfo");
myTamagotchi.report();

