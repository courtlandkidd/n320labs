/**
 * Created by Courtland on 9/7/2017.
 */

Vue.component('dice-roller', {
    template: "<div><div>{{ currentRoll }}</div> <button v-on:click='rollDie'>Roll Dice</button>",
    data: function() {
        return {
            currentRoll: 0
        }
    },
    methods: {
        rollDie: function () {
            this.currentRoll = 1+ Math.floor(Math.random() * 6);
        }
    }
});

//start a new vue application
new Vue({
    el: '#app'
})