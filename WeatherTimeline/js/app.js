/**
 * Created by Courtland on 9/14/2017.
 */
//Weather meter component
Vue.component("weather-meter", {
    template: "#weather-meter-template",
    data: function() {
        return {
            high: 40,
            shownHigh: 40,
            ourFeelings: "Great Scott!"
        }
    },
    created: function () {
        this.interval = setInterval(this.updateWeather, 4000)
    },
    methods: {
        updateWeather: function() {
            this.high = Math.round(Math.random() * 80) + 20;

            if(this.high > 80) {
                this.ourFeelings= "TOO HOT."
            } else if (this.high< 45 ) {
                this.ourFeelings= "TOO COLD.";
            } else {
                this.ourFeelings = "It's weather";
            }
        }
    },
    watch: {
        high: function(newValue, oldValue) {
            TweenMax.to(this, .7, { shownHigh: newValue});
        }
    }

});


//start app
new Vue({
    el: '#app',
    data: function () {
        return {
            showing: true
        }
    }
});
